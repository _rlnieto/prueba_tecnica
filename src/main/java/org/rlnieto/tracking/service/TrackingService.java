package org.rlnieto.tracking.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.rlnieto.tracking.model.Item;
import org.rlnieto.tracking.model.Log;
import org.rlnieto.tracking.model.Tracking;

public class TrackingService {

	// Estados de la orden
	private static final int RECOGIDO = 0;
	private static final int EN_REPARTO = 1;
	private static final int INCIDENCIA = 2;
	private static final int ENTREGADO = 3;
	
	//-------------------------------------------------------------------------------------------------
	// Transiciones válidas: todas menos las que "vuelven" al estado inicial (RECOGIDO)
	// y las que parten del estado final (ENTREGADO) hacia los demás
	//-------------------------------------------------------------------------------------------------
	private static Map<Integer, List<Integer>> transicionesPermitidas = new HashMap<Integer, List<Integer>>(3);
	static {
		transicionesPermitidas.put(RECOGIDO, new ArrayList<Integer>(Arrays.asList(EN_REPARTO, INCIDENCIA, ENTREGADO)));
		transicionesPermitidas.put(EN_REPARTO, new ArrayList<Integer>(Arrays.asList(INCIDENCIA, ENTREGADO)));
		transicionesPermitidas.put(INCIDENCIA, new ArrayList<Integer>(Arrays.asList(EN_REPARTO, ENTREGADO)));
	}
	
	
	/**
	 * Procesa las transiciones que nos han llegado:
	 *   - Valida que es correcta
	 *   - Si es correcta la guarda
	 * 
	 * @param estadoInicial
	 * @return
	 */
	public Response procesarTransiciones(Tracking orderTracking) {

		List<Item>items = orderTracking.getOrderTrackings();

		// Recorremos los items validando las transiciones en función de lo que ha en BD
		for(Item item: items) {
	
			// Leemos el último estado de la BD. Si no lo hay y no es el inicial salimos con error
			Integer estadoInicial = ultimoEstado(item.getOrderId());

			if(estadoInicial < 0 && item.getTrackingStatusId() != RECOGIDO) {
				System.out.println(String.format("La orden %d parte de un estado distinto del inicial (%d)", item.getOrderId(), item.getTrackingStatusId()));
				continue;
			}

			if(this.transaccionValida(estadoInicial, item.getTrackingStatusId())) {
				LogService logService = new LogService();
				logService.nuevoLog(item);
			}
		}
		
		return Response.ok().build();
	}

	
	
	/**
	 * Comprueba si una transición es válida
	 * 
	 * @param estadoInicial
	 * @param estadoFinal
	 * @return
	 */
	private boolean transaccionValida(Integer estadoInicial, Integer estadoFinal) {

		boolean valida = false;

		// TODO: chapucilla para la transición inicial, ver cómo hacerlo más limpio
		if(estadoInicial < 0 && estadoFinal == RECOGIDO) return true;
		
		// Comprobamos si el estado destino enviado está entre los posibles 		
		List<Integer>estadosFinalesPosibles = transicionesPermitidas.get(estadoInicial);

		// El estado no tiene transiciones definidas
		if(null == estadosFinalesPosibles) return false;
		
		// TODO: salir en cuanto lo encuentre (son pocos)
		for(Integer estadoPosible: estadosFinalesPosibles) {
			if(estadoFinal == estadoPosible) valida = true;
		}
		
		return valida;
	}	
	
	/**
	 * Recupera el id de la última entrada en el log para una orden 
	 * @param orderId
	 * @return
	 */
	private int ultimoEstado(int orderId) {
		Log ultimoLog = Log.ultimaEntradaOrden(orderId);
		
		if(null == ultimoLog) return -1;
		
		return ultimoLog.trackingStatusId;

	}
	
	

	
	
	
}
