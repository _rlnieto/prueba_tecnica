package org.rlnieto.tracking.service;

import java.sql.Timestamp;

import org.rlnieto.tracking.model.Item;
import org.rlnieto.tracking.model.Log;

public class LogService {

	public void nuevoLog(Item item) {
    
		Log log = new Log();
    	
		log.orderId = item.getOrderId();
		log.trackingStatusId = item.getTrackingStatusId();
		log.changeStatusDate = item.getChangeStatusDate();
    	log.ts = new Timestamp(System.currentTimeMillis());
    	
    	log.persist();
    	
	}
	
}
