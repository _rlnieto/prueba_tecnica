package org.rlnieto.tracking.model;

public class Item {
	
	private int orderId;
	private int trackingStatusId;
	private String changeStatusDate;


	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getTrackingStatusId() {
		return trackingStatusId;
	}

	public void setTrackingStatusId(int trackingStatusId) {
		this.trackingStatusId = trackingStatusId;
	}

	public String getChangeStatusDate() {
		return changeStatusDate;
	}

	public void setChangeStatusDate(String changeStatusDate) {
		this.changeStatusDate = changeStatusDate;
	}
	
}
