package org.rlnieto.tracking.model;

import java.util.List;

public class Tracking {
	
	private List<Item>orderTrackings;

	
	// Getters
	public List<Item> getOrderTrackings() {
		return orderTrackings;
	}

	public void setOrderTrackings(List<Item> orderTrackings) {
		this.orderTrackings = orderTrackings;
	}
	
}
