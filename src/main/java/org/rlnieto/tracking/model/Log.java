package org.rlnieto.tracking.model;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;


@Entity
public class Log  extends PanacheEntityBase{
	
	@SequenceGenerator(name = "logSeq", sequenceName = "log_id_seq", allocationSize = 1, initialValue = 1)
	@Id @GeneratedValue(generator = "logSeq")public int id;
	public int orderId;
	public int trackingStatusId;
	public String changeStatusDate;
	public Timestamp ts;


	// Devuelve la entrada más reciente para una orden
	public static Log ultimaEntradaOrden(int orderIdBuscado) {

		Map<String, Object> parametros = new HashMap<>();
		parametros.put("orderIdBuscado", orderIdBuscado);
		
		return(find("orderId = :orderIdBuscado order by ts desc", parametros).firstResult());
	}
	
	
}  
