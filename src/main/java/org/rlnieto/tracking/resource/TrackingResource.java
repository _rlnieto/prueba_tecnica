package org.rlnieto.tracking.resource;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.rlnieto.tracking.model.Tracking;
import org.rlnieto.tracking.service.TrackingService;

@Path("/tracking")
public class TrackingResource {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public Response capturaDatos(Tracking orderTracking) {

		TrackingService trackingService = new TrackingService();
		
		return(trackingService.procesarTransiciones(orderTracking));
	}


}